﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaukaSlowek.DataBaseClasses
{
    /// <summary>
    /// Klasa przechowujaca slowko po polsku i jego tlumaczenie 
    /// </summary>
    public class Word
    {
        public int WordId { get; set; }
        public string Polish { get; set; }
        public List<string> English { get; set; }

        /// <summary>
        /// Konwertuje z listy stringow do stringu o formacie (slowo1/slowo2/...)
        /// Konwertuje ze stringu o formacie (slowo1/slowo2/...) do listy stringow
        /// </summary>
        public string EnglishString
        {
            get
            {
                return String.Join("/", English); 
            }
            set
            {
                string[] english = value.Split('/');

                // Usuniecie bialych znakow na poczatku lub koncu
                for (int j = 0; j < english.Count(); ++j)
                {
                    var str = english[j];

                    if (str[0] == ' ' || str[0] == '\t' || str[0] == '\n')
                        str = str.Substring(1);

                    if (str[str.Length - 1] == ' ' || str[str.Length - 1] == '\t' || str[str.Length - 1] == '\n')
                        str = str.Substring(0, str.Length - 1);
                }

                English = new List<string>(english);
            }
        }

        public WordsList WordsList { get; set; }

        public Word() { }

        /// <summary>
        /// Sprawdzenie poprawnosci odpowiedzi
        /// Slowko Polish musi sie zgadzac i English drugiego slowka musi sie zawierac w pierwszym
        /// </summary>
        /// <param name="word">Slowko bedace odpowiedza</param>
        /// <returns>True - prawidlowa odpowiedz, False - zla odpowiedz</returns>
        public bool Correct(Word word)
        {
            if (this.Polish != word.Polish)
                return false;

            return word.English.All(english => this.English.Contains(english));
        }

        /// <summary>
        /// Porownanie wlasciwosci Polish i English
        /// </summary>
        /// <param name="obj">Porownywany obiekt</param>
        /// <returns>True - identyczne wartosci, False - przeciwny wypadek</returns>
        public override bool Equals(object obj)
        {
            var word = obj as Word;

            if (this.Polish != word.Polish)
                return false;

            return this.English.All(english => word.English.Contains(english));
        }

        /// <summary>
        /// Zwraca string w formacie: Polish + " - " + EnglishString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Polish + " - " + EnglishString;
        }
    }
}
