﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaukaSlowek.DataBaseClasses
{
    /// <summary>
    /// Zbior reprezentujacy listy slowek (dzial)
    /// </summary>
    public class Unit
    {
        public int UnitId { get; set; }
        public string UnitName { get; set; }

        public List<WordsList> Lists { get; set; } 


        public Unit() { }

        /// <summary>
        /// Tworzenie plikow dla dzialu w bazie danych
        /// </summary>
        /// <param name="unitName">Nazwa dzialu (Unitu)</param>
        public static void CreateUnit(string unitName)
        {
            using (var file = new StreamWriter(@"DataBase\Units.txt", true))
            {
                file.WriteLine(unitName);
            }

            var nfile = File.Create(@"DataBase\" + unitName + ".txt");
            nfile.Close();
        }

        /// <summary>
        /// Podaje liste wszystkich dzialow w bazie danych
        /// </summary>
        /// <returns>Lista dzialow w bazie danych</returns>
        public static List<string> GiveUnitsList()
        {
            var unitsList = new List<string>();

            using (var read = new StreamReader(@"DataBase\Units.txt"))
            {
                while (!read.EndOfStream)
                {
                    unitsList.Add(read.ReadLine());
                }
            }

            return unitsList;
        }

        /// <summary>
        /// Podaje liste zbiorow w podanym dziale
        /// </summary>
        /// <param name="unitName">Nazwa dzialu</param>
        /// <returns>Lista zbiorow nalezaca do dzialu</returns>
        public static List<string> GiveWordsLists(string unitName)
        {
            var wordsLists = new List<string>();

            using (var reader = new StreamReader(@"DataBase\" + unitName + ".txt"))
            {
                while(!reader.EndOfStream)
                    wordsLists.Add(reader.ReadLine());
            }

            return wordsLists;
        } 

        /// <summary>
        /// Usuwa dzial i wszystkie zbiory slowego do niego nalezace
        /// </summary>
        /// <param name="unitName">Nazwa dzialu</param>
        public static void DeleteUnit(string unitName)
        {
            var wordsLists = GiveWordsLists(unitName);
            var unitsList = GiveUnitsList();

            // Usuniecie z listy dzialow
            using (var writer = new StreamWriter(@"DataBase\Units.txt", false))
            {
                unitsList.Remove(unitName);

                foreach(var unit in unitsList)
                    writer.WriteLine(unit);
            }

            // Usuniecie pliku dzialu
            File.Delete(@"DataBase\" + unitName + ".txt");

            // Usuniecie zbiorow dzialu
            foreach (var wordsList in wordsLists)
            {
                File.Delete(@"DataBase\" + unitName + "_" + wordsList + ".txt");
            }
        }

        /// <summary>
        /// Usuwa zbior/zestaw slowek z danego dzialu
        /// </summary>
        /// <param name="unitName">Nazwa dzialu</param>
        /// <param name="listName">Nazwa zestawu slowek</param>
        public static void DeleteWordsList(string unitName, string listName)
        {
            File.Delete(@"DataBase\" + unitName + "_" + listName + ".txt");

            var wordsLists = GiveWordsLists(unitName);
            wordsLists.Remove(listName);

            using (var writer = new StreamWriter(@"DataBase\" + unitName + ".txt"))
            {
                foreach(var wordsList in wordsLists)
                    writer.WriteLine(wordsList);
            }
        }
    }
}
