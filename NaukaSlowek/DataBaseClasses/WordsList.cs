﻿using System;
using System.Collections.Generic;
using System.IO;

namespace NaukaSlowek.DataBaseClasses
{
    /// <summary>
    /// Zbior reprezentujacy dany zestaw slowek
    /// </summary>
    public class WordsList
    {
        public int WordsListId { get; set; }
        public string ListName { get; set; }

        public List<Word> Words { get; set; }

        public Unit Unit { get; set; }

        public WordsList() { }

        /// <summary>
        /// Tworzy plik na nowy zestaw slowek dla odpowiedniego dzialu
        /// </summary>
        /// <param name="unitName">Nazwa dzialu (Unit)</param>
        /// <param name="listName">Nazwa zestawu</param>
        public static void CreateWordsList(string unitName, string listName)
        {
            using (var writer = new StreamWriter(@"DataBase\" + unitName + ".txt", true))
            {
                writer.WriteLine(listName);
            }

            var file = File.Create(@"DataBase\" + unitName + "_" + listName + ".txt");
            file.Close();
        }

        /// <summary>
        /// Tworzy plik na nowy zestaw slowek dla odpowiedniego dzialu i dodaje do niego slowka
        /// </summary>
        /// <param name="unitName">Nazwa dzialu (Unit)</param>
        /// <param name="listName">Nazwa zestawu</param>
        /// <param name="words">Zestaw slowek</param>
        public static void CreateWordsList(string unitName, string listName, ICollection<Word> words)
        {
            using (var writer = new StreamWriter(@"DataBase\" + unitName + ".txt", true))
            {
                writer.WriteLine(listName);
            }

            var file = File.Create(@"DataBase\" + unitName + "_" + listName + ".txt");
            file.Close();

            AddWords(unitName, listName, words);
        }

        /// <summary>
        /// Wpisuje zestaw slowek do odpowiedniego pliku
        /// </summary>
        /// <param name="unitName">Nazwa dzialu (Unit)</param>
        /// <param name="listName">Nazwa zestawu</param>
        /// <param name="words">Zestaw slowek</param>
        public static void AddWords(string unitName, string listName, ICollection<Word> words)
        {
            using (var writer = new StreamWriter(@"DataBase\" + unitName + "_" + listName + ".txt", true))
            {
                foreach (var word in words)
                {
                    writer.WriteLine(word.Polish + "=" + word.EnglishString);
                }
            }
        }

        /// <summary>
        /// Nadpisuje stary zestaw slowek nowym
        /// </summary>
        /// <param name="unitName">Nazwa dzialu (Unit)</param>
        /// <param name="listName">Nazwa zestawu</param>
        /// <param name="words">Zestaw slowek</param>
        public static void RewriteWordsList(string unitName, string listName, ICollection<Word> words)
        {
            using (var writer = new StreamWriter(@"DataBase\" + unitName + "_" + listName + ".txt", false))
            {
                foreach (var word in words)
                {
                    writer.WriteLine(word.Polish + "=" + word.EnglishString);
                }
            }
        }

        /// <summary>
        /// Odczytuje slowka z pliku
        /// </summary>
        /// <param name="unitName">Nazwa dzialu (Unit)</param>
        /// <param name="listName">Nazwa zestawu</param>
        /// <returns>Zestaw slowek</returns>
        public static List<Word> GetWordsList(string unitName, string listName)
        {
            var wordsList = new List<Word>();

            using (var reader = new StreamReader(@"DataBase\" + unitName + "_" + listName + ".txt", true))
            {
                while (!reader.EndOfStream)
                {
                    string[] line = reader.ReadLine().Split('=');
                    wordsList.Add(new Word() {Polish = line[0], EnglishString = line[1]});
                }
            }

            return wordsList;
        }
    }
}
