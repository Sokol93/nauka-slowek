﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaukaSlowek.DataBaseClasses;

namespace NaukaSlowek.TeachingAlgorythms
{
    public class PartialTeaching : ATeaching
    {
        /// <summary>
        /// Zmienna mowiaca czy nalezy wygenerowac nowa czesc
        /// </summary>
        private bool _newPart = true;
        /// <summary>
        /// Wielkosc czesci do odpytywania
        /// </summary>
        private readonly int _partSize;
        /// <summary>
        /// Generator liczb losowych
        /// </summary>
        private readonly Random _random = new Random();
        /// <summary>
        /// Podaje liczbe losowa z zakresu 0-LiczbaSlowek
        /// </summary>
        protected int Rand => _random.Next() % WordsLeft;
        /// <summary>
        /// Aktualna czesc z ktorej bedzie odbywac sie odpytywanie
        /// </summary>
        private NormalTeaching _partTeaching;

        /// <summary>
        /// Inicjalizacja bazy slowek
        /// </summary>
        /// <param name="wordsList">Zbior slowek</param>
        /// <param name="partSize">Liczba slowek w kazdej czesci</param>
        public PartialTeaching(ICollection<Word> wordsList, int partSize) : base(wordsList)
        {
            _partSize = partSize;
        }

        /// <summary>
        /// Podaje kolejne slowko. Odpytywanie odbywa sie z odpytywania po czesci w ktorych znajduje sie partSize slowek
        /// Przejscie do kolejnej sesji odbywa sie poprzez wywolanie funkcji ContinueTeaching
        /// </summary>
        /// <param name="lastResult">Wynik poprzedniej odpowiedzi</param>
        /// <returns>Kolejne slowko</returns>
        public override Word GiveNext(bool? lastResult = null)
        {
            if (WordsLeft == 0 && _partTeaching.WordsLeft == 0)
                throw new ArgumentOutOfRangeException("Nie ma już słówek do odpytania!");

            if(_newPart)
                _partTeaching = new NormalTeaching(GetNewPart());

            return _partTeaching.GiveNext();
        }

        /// <summary>
        /// Inicjuje generator nowej czesci do odpytywania
        /// </summary>
        public override void ContinueTeaching()
        {
            _newPart = true;
        }

        /// <summary>
        /// Resetuje sesje odpytywania
        /// </summary>
        public override void ResetTeaching()
        {
            _partTeaching.ResetTeaching();
        }

        /// <summary>
        /// Podaje wielkosc czesci z ktorej jest odpytywanie
        /// </summary>
        /// <returns>Wielkosc czesci z ktorej jest odpytywanie</returns>
        public override int NumberOfWords()
        {
            return _partTeaching.Total;
        }

        /// <summary>
        /// Wklada slowka do nowej czesci (losowo wybrane ze wszystkich slowek)
        /// </summary>
        /// <returns>Lista slowek w czesci</returns>
        private List<Word> GetNewPart()
        {
            var part = new List<Word>(_partSize);

            int counter = _partSize;
            while(counter-- > 0 && WordsLeft > 0)
            {
                int index = Rand;
                part.Add(_wordsSession[index]);
                _wordsSession.RemoveAt(index);
            }

            _newPart = false;

            return part;
        }
    }
}
