﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaukaSlowek.DataBaseClasses;

namespace NaukaSlowek.TeachingAlgorythms
{
    /// <summary>
    /// Interfejs odpowiedzialny za sposob odpytywania (trybu odpytywania)
    /// </summary>
    public abstract class ATeaching
    {
        /// <summary>
        /// Referencja do orginalnego zbioru/zestawu slowek
        /// </summary>
        protected readonly ICollection<Word> _wordsOryginal; 
        /// <summary>
        /// Zbior slowek
        /// </summary>
        protected List<Word> _wordsSession;
        /// <summary>
        /// Laczna ilosc slowek w zestawie
        /// </summary>
        public int Total { get; private set; }
        /// <summary>
        /// Pozostala ilosc slowek
        /// </summary>
        public int WordsLeft => _wordsSession.Count;

        /// <summary>
        /// Inicjalizacja bazy slowek
        /// </summary>
        /// <param name="wordsList">Zbior slowek</param>
        protected ATeaching(ICollection<Word> wordsList)
        {
            if(wordsList.Count == 0)
                throw new ArgumentException("Lista nie może być pusta!");

            _wordsOryginal = wordsList;
            _wordsSession = new List<Word>(wordsList);
            Total = _wordsSession.Count;
        }

        /// <summary>
        /// Podanie kolejnego slowka
        /// </summary>
        /// <param name="lastResult">Wynik odpowiedzi na ostatnie zadane slowko (null - brak)</param>
        /// <returns>Kolejne slowko do odpytania</returns>
        public abstract Word GiveNext(bool? lastResult = null);

        /// <summary>
        /// Kontynuacja nauki
        /// </summary>
        public abstract void ContinueTeaching();

        /// <summary>
        /// Resetuje aktualna sesje
        /// </summary>
        public abstract void ResetTeaching();

        /// <summary>
        /// Podaje liczbe slowek z ktorych jest odpytywanie
        /// </summary>
        /// <returns>Liczba slowek z ktorych jest odpytywanie</returns>
        public virtual int NumberOfWords()
        {
            return Total;
        }
    }
}
