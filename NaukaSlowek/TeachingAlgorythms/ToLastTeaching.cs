﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaukaSlowek.DataBaseClasses;

namespace NaukaSlowek.TeachingAlgorythms
{
    /// <summary>
    /// Klasa odpytujaca do ostatniego poprawnego slowka
    /// </summary>
    public class ToLastTeaching : NormalTeaching
    {
        /// <summary>
        /// Indeks ostatnio podanego slowa
        /// </summary>
        private int _lastIndex;

        /// <summary>
        /// Inicjalizacja bazy slowek
        /// </summary>
        /// <param name="wordsList">Zbior slowek</param>
        public ToLastTeaching(ICollection<Word> wordsList) : base(wordsList)
        { }

        /// <summary>
        /// Podaje kolejne slowo. Slowa sa wybierane losowo z tych na ktore nie podano poprawnej odpowiedzi
        /// </summary>
        /// <param name="lastResult">Wynik ostatniej odpowiedzi</param>
        /// <returns>Kolejne Slowo</returns>
        public override Word GiveNext(bool? lastResult = null)
        {
            if (WordsLeft == 0 || (WordsLeft == 1  && lastResult.HasValue && lastResult.Value))
                throw new ArgumentOutOfRangeException("Nie ma już słówek do odpytania!");

            // Jezeli poprawnie odpowiedziano to usuwamy z listy
            if (lastResult.HasValue && lastResult.Value)
            {
                _wordsSession.RemoveAt(_lastIndex);
            }

            return _wordsSession[_lastIndex = Rand];
        }
    }
}
