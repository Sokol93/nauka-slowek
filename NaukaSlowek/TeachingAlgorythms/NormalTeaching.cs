﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaukaSlowek.DataBaseClasses;

namespace NaukaSlowek.TeachingAlgorythms
{
    /// <summary>
    /// Klasa do odpytywania losowo ze slowek (bez powtarzania)
    /// </summary>
    public class NormalTeaching : ATeaching
    {
        /// <summary>
        /// Generator liczb losowych
        /// </summary>
        private readonly Random _random = new Random();
        /// <summary>
        /// Podaje liczbe losowa z zakresu 0-LiczbaSlowek
        /// </summary>
        protected int Rand => _random.Next()%WordsLeft;

        /// <summary>
        /// Inicjalizacja bazy slowek
        /// </summary>
        /// <param name="wordsList">Zbior slowek</param>
        public NormalTeaching(ICollection<Word> wordsList) : base(wordsList)
        {
        }

        /// <summary>
        /// Podaje losowe kolejne slowko losowo wybrane z posrod jeszcze nie wybranych
        /// Wynik odpowiedzi nie ma znaczenia
        /// </summary>
        /// <param name="lastResult">Nie ma znaczenia</param>
        /// <returns>Kolejne slowko do odpytania</returns>
        public override Word GiveNext(bool? lastResult = null)
        {
            if(WordsLeft == 0)
                throw new ArgumentOutOfRangeException("Nie ma już słówek do odpytania!");

            int index = Rand;

            var nextWord = _wordsSession[index];
            _wordsSession.RemoveAt(index);

            return nextWord;
        }

        /// <summary>
        /// Resetuje zbior do odpytywania
        /// </summary>
        public override void ContinueTeaching()
        {
            _wordsSession = new List<Word>(_wordsOryginal);
        }

        /// <summary>
        /// Resetuje zbior do odpytywania 
        /// </summary>
        public override void ResetTeaching()
        {
            ContinueTeaching();
        }
    }
}
