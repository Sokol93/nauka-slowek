﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NaukaSlowek.DataBaseClasses;
using NaukaSlowek.TeachingAlgorythms;

namespace NaukaSlowek.Windows
{
    /// <summary>
    /// Interaction logic for TeachingWindow.xaml
    /// </summary>
    public partial class TeachingWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Klasa odpytujaca
        /// </summary>
        private ATeaching _teaching;
        /// <summary>
        /// Czy odpytywanie ze slowek polskich
        /// </summary>
        private readonly bool _polish;
        /// <summary>
        /// Liczba dobrych odpowiedzi
        /// </summary>
        private int _goodAnsw;
        public int GoodAnswers
        {
            get { return _goodAnsw; }
            set { _goodAnsw = value; OnPropertyChanged("GoodAnswers"); }
        }
        /// <summary>
        /// Liczba złych odpowiedzi
        /// </summary>
        private int _wrongAnsw;
        public int WrongAnswers
        {
            get { return _wrongAnsw; }
            set { _wrongAnsw = value; OnPropertyChanged("WrongAnswers"); }
        }
        /// <summary>
        /// Liczba wszystkich slowek
        /// </summary>
        private int _wordsNum;
        public int WordsNumber
        {
            get { return _wordsNum; }
            set { _wordsNum = value; OnPropertyChanged("WordsNumber"); }
        }
        /// <summary>
        /// Aktualne slowko na ktore udzielana jest odpowiedz
        /// </summary>
        private Word _nextWord;

        /// <summary>
        /// Inicjalizator okna odpytywania
        /// </summary>
        /// <param name="polish">Czy odpytywanie ze slowek polskich</param>
        /// <param name="teaching">Klasa odpytujaca</param>
        public TeachingWindow(bool polish, ATeaching teaching)
        {
            InitializeComponent();
            this.DataContext = this;

            _polish = polish;
            _teaching = teaching;

            if (_polish)
            {
                lblLanguageQ.Content = "Polski:";
                lblLanguageA.Content = "Angielski:";
            }
            else
            {
                lblLanguageQ.Content = "Angielski:";
                lblLanguageA.Content = "Polski:";
            }

            RestartControls();
        }

        #region EVENTS

        /// <summary>
        /// Pokazuje pierwsza litere jako podpowiedz
        /// </summary>
        private void GiveHint_Event(object sender, RoutedEventArgs e)
        {
            txtLanguageA.Text = _polish ? _nextWord.English[0][0].ToString() : _nextWord.Polish[0].ToString();
        }

        /// <summary>
        /// Sprawdzenie poprawnosci odpowiedzi i dodanie jej do listy odpowiedzi. Wygenerowanie nowego slowka
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckAnswer_Event(object sender, RoutedEventArgs e)
        {
            CheckAnswer();
        }

        /// <summary>
        /// Wywoluje metode ContinueTeaching
        /// </summary>
        private void ContinueTeaching_Event(object sender, RoutedEventArgs e)
        {
            if(_teaching.WordsLeft != 0)
                _teaching.ContinueTeaching();
            else
                _teaching.ResetTeaching();

            RestartControls();
        }

        /// <summary>
        /// Wywoluje metode ResetTeaching
        /// </summary>
        private void ResetTeaching_Event(object sender, RoutedEventArgs e)
        {
            _teaching.ResetTeaching();
            RestartControls();
        }

        /// <summary>
        /// Zamyka okno odpytywania
        /// </summary>
        private void EndTeaching_Event(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Wcisniecie przycisku
        /// </summary>
        private void KeyDown_Event(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter && btnAnswer.IsEnabled)
                CheckAnswer();
        }

        #endregion

        #region SUPPORTING FUNCTIONS

        /// <summary>
        /// Pobiera kolejne slowko z ktorego bedzie odpytywanie i pokazuje je na ekranie
        /// </summary>
        private void AskNextWord(bool? lastResult = null)
        {
            _nextWord = _teaching.GiveNext(lastResult);

            txtLanguageQ.Text = _polish ? _nextWord.Polish : _nextWord.EnglishString;
            txtLanguageA.Text = "";
        }

        /// <summary>
        /// Sprawdzenie poprawnosci odpowiedzi i dodanie jej do listy odpowiedzi. Wygenerowanie nowego slowka
        /// </summary>
        private void CheckAnswer()
        {
            var answer = new Word()
            {
                Polish = _polish ? txtLanguageQ.Text : txtLanguageA.Text,
                EnglishString = _polish ? txtLanguageA.Text : txtLanguageQ.Text,
            };

            bool result = _nextWord.Correct(answer);

            if (result)
            {
                GoodAnswers++;
                lstAnswers.Items.Add(new TextBlock() { Text = _nextWord.ToString(), Foreground = Brushes.Green });
            }
            else
            {
                WrongAnswers++;
                lstAnswers.Items.Add(new TextBlock() { Text = _nextWord.ToString(), Foreground = Brushes.Red });
            }

            try
            {
                AskNextWord(result);
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show(
                    "Zakończono odpytywanie Twój wynik:\n\nPoprawne odpowiedzi: " + GoodAnswers +
                    "\n\nZłe odpowiedzi: " + WrongAnswers + "\n\nLiczba wszystkich slowek: " + WordsNumber,
                    "Koniec odpytywania");

                btnAnswer.IsEnabled = false;
            }
        }

        /// <summary>
        /// Resetowanie wszystkich kontrolek do kolejnego odpytywania
        /// </summary>
        private void RestartControls()
        {
            AskNextWord();

            GoodAnswers = WrongAnswers = 0;
            WordsNumber = _teaching.NumberOfWords();

            btnAnswer.IsEnabled = true;
            lstAnswers.Items.Clear();

            txtLanguageA.Focus();
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
