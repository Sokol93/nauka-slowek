﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NaukaSlowek.DataBaseClasses;

namespace NaukaSlowek.Windows
{
    /// <summary>
    /// Interaction logic for DeleteUnitWindow.xaml
    /// </summary>
    public partial class DeleteUnitWindow : Window
    {
        /// <summary>
        /// Lista dzialow (unitow)
        /// </summary>
        public ObservableCollection<string> Units { get; set; }

        public DeleteUnitWindow()
        {
            InitializeComponent();
            this.DataContext = this;

            Units = new ObservableCollection<string>(Unit.GiveUnitsList());
        }

        #region EVENTS

        /// <summary>
        /// Akceptacja usunięcia zaznaczonego dzialu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AcceptDeleting_Event(object sender, RoutedEventArgs e)
        {
            if (lstUnits.SelectedIndex != -1 
                && MessageBoxResult.Yes == MessageBox.Show("Potwierdź chęć usunięcia CAŁEJ ZAWARTOŚCI działu: " + lstUnits.SelectedItem, "Potwierdzenie usunięcia działu", MessageBoxButton.YesNo))
            {
                Unit.DeleteUnit(lstUnits.SelectedItem as string);
                this.Close();
            }
            else
            {
                MessageBox.Show("Nie zanaczono działu do usunięcia!");
            }
        }

        /// <summary>
        /// Anuluje akcje usuniecia dzialu. Zamyka okno
        /// </summary>
        private void CancelDeleting_Event(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}
