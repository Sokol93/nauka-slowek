﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NaukaSlowek.DataBaseClasses;

namespace NaukaSlowek.Windows
{
    /// <summary>
    /// Interaction logic for DeleteWordsListWindow.xaml
    /// </summary>
    public partial class SelectUnitAndWordsListWindow : Window
    {
        /// <summary>
        /// Zbior dzialow (unitow)
        /// </summary>
        public ObservableCollection<string> Units { get; set; } 
        /// <summary>
        /// Zbior zestawow slowek danego unitu
        /// </summary>
        public ObservableCollection<string> WordsLists { get; set; } 
        /// <summary>
        /// Czy zaakceptowano wybor dzialu i zbioru slowek
        /// </summary>
        public bool Result { get; private set; }
        /// <summary>
        /// Wyniki zaznaczenia
        /// </summary>
        public KeyValuePair<string,string> UnitAndWordsList { get; private set; } 
        
        public SelectUnitAndWordsListWindow()
        {
            InitializeComponent();
            this.DataContext = this;

            Units = new ObservableCollection<string>(Unit.GiveUnitsList());
            WordsLists = new ObservableCollection<string>();

            Result = false;
        }

        #region EVENTS

        /// <summary>
        /// Gdy zmieniony zostal wskazany unit, aktualizujemy liste jego zestawow slowek
        /// </summary>
        private void SelectedUnitChanged_Event(object sender, SelectionChangedEventArgs e)
        {
            if (lstUnits.SelectedIndex != -1)
            {
                WordsLists.Clear();

                var wordsLists = Unit.GiveWordsLists(lstUnits.SelectedItem as string);
                foreach (var wordsList in wordsLists)
                {
                    WordsLists.Add(wordsList);
                }
            }
            else
                WordsLists.Clear();
        }

        /// <summary>
        /// Akceptacja usuniecia podanego zestawu slowek - usuniecie go z listy
        /// </summary>
        private void AcceptDeletingWordsList_Event(object sender, RoutedEventArgs e)
        {
            if (lstUnits.SelectedIndex != -1 && lstWordsLists.SelectedIndex != -1
                &&
                MessageBoxResult.Yes ==
                MessageBox.Show("Potwierdz wybór zestawu " + lstWordsLists.SelectedItem + " z działu " + lstUnits.SelectedItem,
                    "Potwierdzenie wyboru", MessageBoxButton.YesNo))
            {
                Result = true;
                UnitAndWordsList = new KeyValuePair<string, string>(lstUnits.SelectedItem as string, lstWordsLists.SelectedItem as string);

                this.Close();
            }
            else
            {
                MessageBox.Show("Niepoprawnie wybrano zbiór do usunięcia!");
            }
        }

        /// <summary>
        /// Anulowanie usuniecia zestawu slowek. Zamkniecie okna
        /// </summary>
        private void CancelDeletingWordsList_Event(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}
