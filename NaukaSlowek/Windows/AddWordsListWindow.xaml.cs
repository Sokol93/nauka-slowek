﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NaukaSlowek.DataBaseClasses;

namespace NaukaSlowek.Windows
{
    /// <summary>
    /// Interaction logic for AddWordsListWindow.xaml
    /// </summary>
    public partial class AddWordsListWindow : Window
    {
        /// <summary>
        /// Dzialy slowek do wyswietlenia
        /// </summary>
        public ObservableCollection<string> Units { get; set; }
        /// <summary>
        /// Zbior slowek dodanych do zestawu
        /// </summary>
        public ObservableCollection<KeyValuePair<string, string>> WordsPair { get; set; } 

        public AddWordsListWindow()
        {
            InitializeComponent();
            this.DataContext = this;

            Units = new ObservableCollection<string>(Unit.GiveUnitsList());
            WordsPair = new ObservableCollection<KeyValuePair<string, string>>();

            pnlAddNewUnit.Visibility = Visibility.Collapsed;     
        }

        #region EVENTS

        /// <summary>
        /// Pokazuje panel dodawania nowego dzialu
        /// </summary>
        private void AddNewUnit_Event(object sender, RoutedEventArgs e)
        {
            pnlAddNewUnit.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Akceptacja dodania nowego działu
        /// Dodanie dzialu do listy dzialow, wyczyszczenie kontroliki z nazwa, schowanie panelu dodawania dzialu
        /// </summary>
        private void AcceptAddNewUnit_Event(object sender, RoutedEventArgs e)
        {
            if (txtUnitName.Text != "")
            {
                Units.Add(txtUnitName.Text);
                Unit.CreateUnit(txtUnitName.Text);

                txtUnitName.Text = "";
                pnlAddNewUnit.Visibility = Visibility.Collapsed;
            }
            else
            {
                MessageBox.Show("Nazwa działu nie może być pusta!");
            }
        }

        /// <summary>
        /// Anulowanie dodania nowego działu
        /// Wyczyszczenie kontroliki z nazwa, schowanie panelu dodawania dzialu
        /// </summary>
        private void CancelAddNewUnit_Event(object sender, RoutedEventArgs e)
        {
            txtUnitName.Text = "";
            pnlAddNewUnit.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Dodanie nowego slowka (pary: polskie-agnielskie) do zestawu.
        /// Wyczyszczenie kontrolek slowek i ustawienie focusu na slowko polskie
        /// </summary>
        private void AddNewWordPair_Event(object sender, RoutedEventArgs e)
        {
            AddNewWordPair();
        }

        /// <summary>
        /// Usuniecie zaznaczonej pary slowek
        /// </summary>
        private void RemoveSelectedWordPair_Event(object sender, RoutedEventArgs e)
        {
            int i = lstWords.SelectedIndex;

            if (i != -1
                && MessageBoxResult.Yes == MessageBox.Show("Potwierdź chęć usunięcia (" + WordsPair[i].Key + "," + WordsPair[i].Value + ")", "Potwierdzenie usunięcia słówka", MessageBoxButton.YesNo))
            {
                WordsPair.RemoveAt(i);
            }
        }

        /// <summary>
        /// Dodanie nowego slowka (pary: polskie-agnielskie) do zestawu.
        /// Wyczyszczenie kontrolek slowek i ustawienie focusu na slowko polskie
        /// </summary>
        private void OnKeyDown_Event(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AddNewWordPair();
            }
        }

        /// <summary>
        /// Anulowanie dodawania nowego zestawu slowek - zamkniecie okna
        /// </summary>
        private void CancelAddWordsList_Event(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Wpisuje wszystkie dodane slowka do danego zestawu do bazy danych
        /// </summary>
        private void AcceptAddWordsList_Event(object sender, RoutedEventArgs e)
        {
            if (txtListName.Text != "" && lstUnits.SelectedIndex != -1 && WordsPair.Count > 0)
            {
                var wordsList = WordsPair.Select(pair => new Word() {Polish = pair.Key, EnglishString = pair.Value}).ToArray();

                WordsList.CreateWordsList(lstUnits.SelectedItem as string, txtListName.Text, wordsList);

                this.Close();
            }
            else
            {
                MessageBox.Show("Nie można dodać zestawu. Możliwe przyczyny błędu:\n-Pole z nazwą jest puste\n-Nie wybrano działu\n-Nie dodano słówek do zestawu");
            }
        }

        #endregion

        #region SUPPORTING FUNCTIONS

        /// <summary>
        /// Dodanie nowego slowka (pary: polskie-agnielskie) do zestawu.
        /// Wyczyszczenie kontrolek slowek i ustawienie focusu na slowko polskie
        /// </summary>
        private void AddNewWordPair()
        {
            if (txtPolish.Text != "" && txtEnglish.Text != "")
            {
                WordsPair.Add(new KeyValuePair<string, string>(txtPolish.Text, txtEnglish.Text));

                txtPolish.Text = txtEnglish.Text = "";
                txtPolish.Focus();
            }
            else
            {
                MessageBox.Show("Pola ze słówkiem po polsku i po angielsku nie mogą być puste!");
            }
        }

        #endregion
    }
}
