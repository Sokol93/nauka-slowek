﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NaukaSlowek.DataBaseClasses;

namespace NaukaSlowek.Windows
{
    /// <summary>
    /// Interaction logic for EditWordsListWindow.xaml
    /// </summary>
    public partial class EditWordsListWindow : Window
    {
        /// <summary>
        /// Lista slowek ulozona w pary
        /// </summary>
        public ObservableCollection<KeyValuePair<string, string>> Words { get; set; }
        /// <summary>
        /// Lista slowek
        /// </summary>
        private List<Word> _wordsList;
        /// <summary>
        /// Czy uruchomiono operacje dodawania slowka
        /// </summary>
        private bool _addEvent;
        /// <summary>
        /// Indeks elementu ktory poddawany jest okreslonej akcji
        /// </summary>
        private int _index;
        /// <summary>
        /// Nazwa dzialu z ktorego jest edytowany zestaw slowek
        /// </summary>
        private readonly string _unitName;
        /// <summary>
        /// Nazwa zbioru/zestawu slowek ktory jest edytowany
        /// </summary>
        private readonly string _listName;

        /// <summary>
        /// Inicjalizator ekranu
        /// </summary>
        /// <param name="unitName">Nazwa dzialu</param>
        /// <param name="listName">Nazwa zbioru/zestawu slowek</param>
        public EditWordsListWindow(string unitName, string listName)
        {
            InitializeComponent();
            this.DataContext = this;

            _unitName = unitName;
            _listName = listName;

            _wordsList = WordsList.GetWordsList(unitName, listName);
            Words =
                new ObservableCollection<KeyValuePair<string, string>>(
                    _wordsList.Select(word => new KeyValuePair<string, string>(word.Polish, word.EnglishString)));
        }

        #region EVENTS

        /// <summary>
        /// Uruchamia operacje dodawania nowego slowka do listy
        /// </summary>
        private void AddNewWord_Event(object sender, RoutedEventArgs e)
        {
            ChangeEnableStatusAndClear(true);

            _addEvent = true;
        }

        /// <summary>
        /// Uruchamia operacje zmiany zaznaczonego slowka slowka
        /// </summary>
        private void ChangeWord_Event(object sender, RoutedEventArgs e)
        {
            if (lstWords.SelectedIndex != -1)
            {
                _index = lstWords.SelectedIndex;
                ChangeEnableStatusAndClear(true);

                txtPolish.Text = Words[_index].Key;
                txtEnglish.Text = Words[_index].Value;

                _addEvent = false;
            }
            else
            {
                MessageBox.Show("Nie zaznaczono żadnego słówka!");
            } 
        }

        /// <summary>
        /// Usuwa slowko z listy
        /// </summary>
        private void RemoveWord_Event(object sender, RoutedEventArgs e)
        {
            int index = lstWords.SelectedIndex;

            if (index != -1 &&
                MessageBoxResult.Yes == MessageBox.Show("Czy napewno chcesz usunąć słówko: " + Words[index].Key + " - " + Words[index].Value + "?", "Potwierdź usunięcie słówka", MessageBoxButton.YesNo))
            {
                Words.RemoveAt(index);
                _wordsList.RemoveAt(index);
            }
            else
            {
                MessageBox.Show("Nie zaznaczono żadnego słówka!");
            }
        }

        /// <summary>
        /// Akceptuje wynik akcji zmiany/dodawania slowka
        /// </summary>
        private void Accept_Event(object sender, RoutedEventArgs e)
        {
            if (txtPolish.Text == "" || txtEnglish.Text == "")
            {
                MessageBox.Show("Pole ze słówkiem polskim i angielskim nie mogą być puste!");
                return;
            }

            var pl = txtPolish.Text;
            var eng = txtEnglish.Text;

            if (_addEvent)
            {
                Words.Add(new KeyValuePair<string, string>(pl, eng));
                _wordsList.Add(new Word() { Polish = pl, EnglishString = eng});
            }
            else
            {
                Words[_index] = new KeyValuePair<string, string>(pl, eng);
                _wordsList[_index] = new Word() { Polish = pl, EnglishString = eng };
            }

            ChangeEnableStatusAndClear(false);
        }

        /// <summary>
        /// Akceptuje wprowadzone zmiany w pliku
        /// </summary>
        private void AcceptChanges_Event(object sender, RoutedEventArgs e)
        {
            if (MessageBoxResult.Yes ==
                MessageBox.Show("Czy na pewno chcesz wprowadzić zmiany w pliku?", "Potwierdzenie wprowadzenia zmian",
                    MessageBoxButton.YesNo))
            {
                WordsList.RewriteWordsList(_unitName, _listName, _wordsList);
                this.Close();
            }
        }

        /// <summary>
        /// Anuluje zmiany wprowadzone w pliku i wychodzi z programu
        /// </summary>
        private void CancelChanges_Event(object sender, RoutedEventArgs e)
        {
            if (MessageBoxResult.Yes ==
                MessageBox.Show("Czy na pewno chcesz przucić wprowadzone zmiany w pliku?", "Potwierdzenie porzucenia zmian",
                    MessageBoxButton.YesNo))
            {
                this.Close();
            }
        }

        #endregion

        #region SUPPORTING FUNCTIONS

        /// <summary>
        /// Zmienia status dostepnosci kontrolek do zmiany/dodawania slowa i czysci ich zawartosci
        /// </summary>
        /// <param name="isEnabled">Status aktywnosci (true-aktywny)</param>
        private void ChangeEnableStatusAndClear(bool isEnabled)
        {
            txtPolish.IsEnabled = txtEnglish.IsEnabled = btnAccept.IsEnabled = isEnabled;
            txtPolish.Text = txtEnglish.Text = "";
        }

        #endregion


    }
}
