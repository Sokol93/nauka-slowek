﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using NaukaSlowek.DataBaseClasses;
using NaukaSlowek.TeachingAlgorythms;

namespace NaukaSlowek.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Baza danych ze slowkami, dzialami, zestawami slowek
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            InitializeDataBase();

            pnlTeaching.Visibility = Visibility.Collapsed;
            pnlWordsBase.Visibility = Visibility.Collapsed;    
        }

        #region EVENTS

        /// <summary>
        /// Pokazanie panelu nauki
        /// </summary>
        private void ExpandTeachingPanel_Event(object sender, RoutedEventArgs e)
        {
            pnlTeaching.Visibility = Visibility.Visible;
            pnlWordsBase.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Pokazanie panelu plikow bazy danych
        /// </summary>
        private void ExpandWordsBasePanel_Event(object sender, RoutedEventArgs e)
        {
            pnlWordsBase.Visibility = Visibility.Visible;
            pnlTeaching.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Zamkniecie wszystkich okien programu. Wyjscie z programu
        /// </summary>
        private void ExitProgram_Event(object sender, RoutedEventArgs e)
        {
            foreach (var window in this.OwnedWindows)
            {
                ((Window)window).Close();
            }

            this.Close();
        }

        /// <summary>
        /// Tworzy nowy plik ze slowkami (liste slowek)
        /// </summary>
        private void AddNewWordsList_Event(object sender, RoutedEventArgs e)
        {
            var window = new AddWordsListWindow();
            window.ShowDialog();
        }

        /// <summary>
        /// Uruchamia okno usuwania dzialu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteUnit_Event(object sender, RoutedEventArgs e)
        {
            var window = new DeleteUnitWindow();
            window.ShowDialog();
        }

        /// <summary>
        /// Uruchamia okno do wyboru dzialu i zbioru slowek, a po pomyslnym ich wyborze usuwa plik
        /// </summary>
        private void DeleteWordsList_Event(object sender, RoutedEventArgs e)
        {
            var window = new SelectUnitAndWordsListWindow();
            window.ShowDialog();

            if (window.Result)
            {
                Unit.DeleteWordsList(window.UnitAndWordsList.Key, window.UnitAndWordsList.Value);
            }
        }

        /// <summary>
        /// Uruchomienie trybu odpytywania normalnego ze slowek
        /// </summary>
        private void NormalTeaching_Event(object sender, RoutedEventArgs e)
        {
            var window = new TeachingSpecificationWindow(false);
            window.ShowDialog();

            if (window.ResultList.Count != 0)
            {
                var teachingWindow = new TeachingWindow(window.ResultLanguagePolish, new NormalTeaching(window.ResultList));
                teachingWindow.ShowDialog();
            }
        }

        /// <summary>
        /// Uruchomienie trybu odpytywania do ostatniej poprawnej ze slowek
        /// </summary>
        private void ToLastTeaching_Event(object sender, RoutedEventArgs e)
        {
            var window = new TeachingSpecificationWindow(false);
            window.ShowDialog();

            if (window.ResultList.Count != 0)
            {
                var teachingWindow = new TeachingWindow(window.ResultLanguagePolish, new ToLastTeaching(window.ResultList));
                teachingWindow.ShowDialog();
            }
        }

        /// <summary>
        /// Uruchomienie trybu odpytywania po czesci ze slowek
        /// </summary>
        private void PartialTeaching_Event(object sender, RoutedEventArgs e)
        {
            var window = new TeachingSpecificationWindow(true);
            window.ShowDialog();

            if (window.ResultList.Count != 0)
            {
                var teachingWindow = new TeachingWindow(window.ResultLanguagePolish, new PartialTeaching(window.ResultList, window.ResultPartSize));
                teachingWindow.ShowDialog();
            }
        }

        /// <summary>
        /// Uruchamia okno do zmiany zestawu slowek
        /// </summary>
        private void ChangeWordsList_Event(object sender, RoutedEventArgs e)
        {
            var window = new SelectUnitAndWordsListWindow();
            window.ShowDialog();

            if (window.Result)
            {
                var editWordsListWindow = new EditWordsListWindow(window.UnitAndWordsList.Key, window.UnitAndWordsList.Value);
                editWordsListWindow.ShowDialog();
            }
        }

        #endregion

        #region OTHER FUNCTIONS

        /// <summary>
        /// Inicjalizacja bazy danych slowek. Tworzy folder DataBase oraz plik na dzialy (Units)
        /// </summary>
        private void InitializeDataBase()
        {
            if (!Directory.Exists("DataBase"))
            {
                Directory.CreateDirectory("DataBase");

                var file = File.Create(@"DataBase\Units.txt");
                file.Close();

                Unit.CreateUnit("Default");
            }
        }

        #endregion
    }
}
