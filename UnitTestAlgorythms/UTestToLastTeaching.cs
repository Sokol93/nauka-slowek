﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NaukaSlowek.DataBaseClasses;
using NaukaSlowek.TeachingAlgorythms;

namespace UnitTestAlgorythms
{
    [TestClass]
    public class UTestToLastTeaching
    {
        /// <summary>
        /// Sprawdzenie, czy funkcja zwroci wszystkie slowa jezeli odpowiedziano na nie poprawnie
        /// </summary>
        [TestMethod]
        public void GiveNext_GoodAnswers_EmptyTeachingList()
        {
            var wordsList = new Word[]
            {
                new Word() {Polish = "pl1", EnglishString = "ang1"},
                new Word() {Polish = "pl2", EnglishString = "ang2"},
                new Word() {Polish = "pl3", EnglishString = "ang3"},
                new Word() {Polish = "pl4", EnglishString = "ang4"},
                new Word() {Polish = "pl5", EnglishString = "ang5"},
            };

            int count = wordsList.Count();
            var teaching = new ToLastTeaching(wordsList);

            teaching.GiveNext();
            for (int i = 0; i < count-1; ++i)
                teaching.GiveNext(true);

            Assert.IsTrue(teaching.WordsLeft == 1);
        }

        /// <summary>
        /// Sprawdzenie, czy zostanie wyrzucony wyjatek po probie przekroczenia ilosci slowek w zestawie
        /// (zbyt duza liczba wywolan GiveNext)
        /// </summary>
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestMethod]
        public void GiveNext_TooManyGiveNextCalls_ExceptionThrown()
        {
            var wordsList = new Word[]
            {
                new Word() {Polish = "pl1", EnglishString = "ang1"},
            };

            int count = wordsList.Count();
            var teaching = new ToLastTeaching(wordsList);

            teaching.GiveNext();
            for (int i = 0; i < count; ++i)
                teaching.GiveNext(true);

            Assert.IsTrue(teaching.WordsLeft == 0);
        }

        /// <summary>
        /// Sprawdzenie, czy zwracane sa wszystkie slowka z listy
        /// </summary>
        [TestMethod]
        public void GiveNext_WordsList_GaveAllWords()
        {
            var expected = new Word[]
            {
                new Word() {Polish = "pl1", EnglishString = "ang1"},
                new Word() {Polish = "pl2", EnglishString = "ang2"},
                new Word() {Polish = "pl3", EnglishString = "ang3"},
                new Word() {Polish = "pl4", EnglishString = "ang4"},
                new Word() {Polish = "pl5", EnglishString = "ang5"},
            };

            int count = expected.Count();
            var teaching = new ToLastTeaching(expected);

            var actual = new List<Word>(count);

            actual.Add(teaching.GiveNext());
            for (int i = 0; i < count-1; ++i)
            {
                actual.Add(teaching.GiveNext(true));
            }
                
            CollectionAssert.AreEquivalent(expected, actual);
        }

        /// <summary>
        /// Sprawdzenie, czy sesja jest prawidlowo 
        /// </summary>
        [TestMethod]
        public void ContinueTeaching_FalseOrNullAnswer_NoWordRemoved()
        {
            var wordsList = new Word[]
            {
                new Word() {Polish = "pl1", EnglishString = "ang1"},
            };

            int expected = wordsList.Count();
            var teaching = new ToLastTeaching(wordsList);

            teaching.GiveNext();
            teaching.GiveNext(false);

            Assert.AreEqual(expected, teaching.WordsLeft);
        }
    }
}
