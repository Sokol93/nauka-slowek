﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NaukaSlowek.DataBaseClasses;
using NaukaSlowek.TeachingAlgorythms;

namespace UnitTestAlgorythms
{
    [TestClass]
    public class UTestPartialTeaching
    {
        /// <summary>
        /// Sprawdzenie, czy funkcja zwroci wszystkie slowa
        /// </summary>
        [TestMethod]
        public void GiveNext_WordsList_EmptyTeachingList()
        {
            var wordsList = new Word[]
            {
                new Word() {Polish = "pl1", EnglishString = "ang1"},
                new Word() {Polish = "pl2", EnglishString = "ang2"},
                new Word() {Polish = "pl3", EnglishString = "ang3"},
                new Word() {Polish = "pl4", EnglishString = "ang4"},
                new Word() {Polish = "pl5", EnglishString = "ang5"},
            };

            int count = wordsList.Count();
            var teaching = new PartialTeaching(wordsList, count);

            for (int i = 0; i < count; ++i)
                teaching.GiveNext();

            Assert.IsTrue(teaching.WordsLeft == 0);
        }

        /// <summary>
        /// Sprawdzenie, czy zostanie wyrzucony wyjatek po probie przekroczenia ilosci slowek w zestawie
        /// (zbyt duza liczba wywolan GiveNext)
        /// </summary>
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestMethod]
        public void GiveNext_TooManyGiveNextCalls_ExceptionThrown()
        {
            var wordsList = new Word[]
            {
                new Word() {Polish = "pl1", EnglishString = "ang1"},
            };

            int count = wordsList.Count();
            var teaching = new PartialTeaching(wordsList, count);

            for (int i = 0; i < count+1; ++i)
                teaching.GiveNext();

            Assert.IsTrue(teaching.WordsLeft == 0);
        }

        /// <summary>
        /// Sprawdzenie, czy zwracane sa wszystkie slowka z listy
        /// </summary>
        [TestMethod]
        public void GiveNext_WordsList_GaveAllWords()
        {
            var expected = new Word[]
            {
                new Word() {Polish = "pl1", EnglishString = "ang1"},
                new Word() {Polish = "pl2", EnglishString = "ang2"},
                new Word() {Polish = "pl3", EnglishString = "ang3"},
                new Word() {Polish = "pl4", EnglishString = "ang4"},
                new Word() {Polish = "pl5", EnglishString = "ang5"},
            };

            int count = expected.Count();
            var teaching = new PartialTeaching(expected, count);

            var actual = new List<Word>(count);

            for (int i = 0; i < count; ++i)
            {
                actual.Add(teaching.GiveNext());
            }
                
            CollectionAssert.AreEquivalent(expected, actual);
        }

        /// <summary>
        /// Sprawdzenie, czy sesja prawidlowo jest resetowana
        /// </summary>
        [TestMethod]
        public void ResetTeaching_WordsList_SessionCorrectlyReseted()
        {
            var wordsList = new Word[]
            {
                new Word() {Polish = "pl1", EnglishString = "ang1"},
                new Word() {Polish = "pl2", EnglishString = "ang2"},
                new Word() {Polish = "pl3", EnglishString = "ang3"},
                new Word() {Polish = "pl4", EnglishString = "ang4"},
                new Word() {Polish = "pl5", EnglishString = "ang5"},
            };

            int partSize = 2;
            var teaching = new PartialTeaching(wordsList, partSize);

            var session1 = new Word[2];
            var session2 = new Word[2];

            for (int i = 0; i < partSize; ++i)
                session1[i] = teaching.GiveNext();

            teaching.ResetTeaching();

            for (int i = 0; i < partSize; ++i)
                session2[i] = teaching.GiveNext();

            CollectionAssert.AreEquivalent(session1, session2);
        }

        /// <summary>
        /// Sprawdzenie, czy sesja prawidlowo jest resetowana
        /// </summary>
        [TestMethod]
        public void ContinueTeaching_WordsList_SessionsAreDifferent()
        {
            var wordsList = new Word[]
            {
                new Word() {Polish = "pl1", EnglishString = "ang1"},
                new Word() {Polish = "pl2", EnglishString = "ang2"},
            };

            int partSize = 1;
            var teaching = new PartialTeaching(wordsList, partSize);

            var session1 = teaching.GiveNext();

            teaching.ContinueTeaching();

            var session2 = teaching.GiveNext();

            Assert.AreNotEqual(session1, session2);
        }

    }
}
